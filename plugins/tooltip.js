import Vue from 'vue'
import VTooltip from 'v-tooltip'
import '@/assets/css/tooltip.scss'

Vue.use(VTooltip, {
    disposeTimeout: 300
})